from  requests_html import HTMLSession
from bs4 import BeautifulSoup

s = HTMLSession()
url = "https://www.amazon.co.uk/s?k=dslr"

def get_page_info(url):
	r = s.get(url)
	soup = BeautifulSoup(r.text,'html.parser')
	return soup


def get_pagination(soup):
	page = soup.find('ul' ,{'class':'a-pagination'})

	if page is None:
	        return
	elif page.find('li',{'class':'a-normal'}):
		url = 'http:///www.amazon.co.uk' + str(page.find('li',{'class':'a-normal'}).find('a')['href'])
		return url
	else:
		return


while True:
	soup = get_page_info(url)
	res = get_pagination(soup)

	if url:
		x = s.get(url)
		f = BeautifulSoup(x.text,'html.parser')
		print(f)
		with open('data.txt','w') as file:
			file.write(str(f))
	else:
		break
