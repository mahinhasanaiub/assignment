from bs4 import BeautifulSoup
import requests
from urllib.parse import urlparse
from urllib.parse import urljoin



URL = "https://www.ebay.com/b/Laptops-Netbooks/175672/bn_1648276?rt=nc&_pgn=1&fbclid=IwAR2cfGkWpNB-5Y2uKylBefARF6FLyznwEJL46SqtoS5zqMP_7OEXg0JnDNM"
s = requests.Session()

def fetch(url, data=None):
    if data is None:
        return s.get(url).content
    else:
        return s.post(url, data=data).content

soup = BeautifulSoup(fetch(URL))
form = soup.find('form')
fields = form.findAll('input')

formdata = dict( (field.get('name'), field.get('value')) for field in fields)

formdata['username'] = u'username'
formdata['password'] = u'password'

print(formdata)
